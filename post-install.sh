#! /usr/bin/env bash

# Stop on error
set -e

echo 'Finish installing touch gestures'
(cd /opt/libinput-gestures && sudo make install)
libinput-gestures-setup autostart
libinput-gestures-setup start
