#! /usr/bin/env bash

# Stop on error
set -e

echo 'Installing sources'
echo '------------------'
sudo apt-get install -y ca-certificates curl software-properties-common
sudo mkdir -p /etc/apt/keyrings

echo 'Adding Docker source'
wget -q0 - https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo 'Installing packages'
echo '-------------------'
sudo apt-get update
sudo apt-get install -y \
	ansible `# ansistrano` \
	byobu \
	build-essential \
	docker-ce \
	flatpak gnome-software-plugin-flatpak \
	gnome-tweaks \
	stow \
	ubuntu-restricted-extras

echo 'Setting up flatpak remote'
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo 'Installing flatpak packages'
flatpak install com.slack.Slack

echo 'Setting up Linuxbrew'
bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo "export PATH='/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:\$PATH'" >> ~/.profile # TODO: we don't want the $PATH to be parsed here!
export PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH"

echo 'Installing brew packages'
brew install \
  ctop `# docker container usage` \
  tealdeer

echo 'Installing docker-compose'
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-`uname -s`-`uname -m`" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo 'Set up docker so unprivileged users can run it'
sudo usermod -aG docker $USER

echo 'Installing pause-on-lock'
sudo curl -L "https://github.com/folixg/pause-on-lock/releases/latest/download/pause-on-lock" -o /usr/local/bin/pause-on-lock
sudo chmod +x /usr/local/bin/pause-on-lock

echo 'Setting up config files'
mkdir ~/.byobu # this is just so byobu config files are symlinked individually, not the entire directory
stow byobu
sudo stow --target=/etc etc

echo 'Installing docker developer hosts'
echo '---------------------------------'
sudo systemctl enable docker.developer-hosts.service
sudo systemctl start docker.developer-hosts.service

echo 'Set some default settings'
gsettings set org.gnome.desktop.interface show-battery-percentage true

echo 'Adding some default directories for the docker volume mounts'
mkdir ~/.composer ~/.npm