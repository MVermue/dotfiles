# clone antidote if necessary
if ! [[ -e ${ZDOTDIR:-~}/.antidote ]]; then
  git clone https://github.com/mattmc3/antidote.git ${ZDOTDIR:-~}/.antidote
fi

# source antidote and load plugins from `${ZDOTDIR:-~}/.zsh_plugins.txt`
zstyle ':antidote:bundle' use-friendly-names 'yes'
source ${ZDOTDIR:-~}/.antidote/antidote.zsh
antidote load

# Things that change the path must come BEFORE p10k instant prompt
source $HOME/.profile

# Don't prompt setting up powerlevel10k in case of issues loading the theme settings
POWERLEVEL9K_DISABLE_CONFIGURATION_WIZARD=true

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

zstyle ':prezto:*:*' color 'yes'

[ -f ~/.bash_aliases ] && source ~/.bash_aliases

export EDITOR=nano
#autoload -U +X bashcompinit && bashcompinit
#complete -o nospace -C /home/linuxbrew/.linuxbrew/bin/bit-git bit

# To customize prompt, run `p10k configure` or edit $ZDOTDIR/.p10k.zsh.
autoload -Uz promptinit && promptinit && prompt powerlevel10k

# Good source: https://github.com/marlonrichert/.config/blob/35910d7f6cdb8d043cdea6226b9b12c41419b1de/zsh/.zshrc
