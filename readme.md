# Dotfiles

Installing the dotfiles is done using GNU stow, so you can install exactly the parts you need.

If you're on Windows (WSL), you'll also need to execute the following command in powershell:  
`Install-Module -Name BurntToast`

# TODO

* New docker hosts dns (from dockerwest)
* Add vim-plugged git subrepo
* Replace solaar with [logips](https://github.com/PixlOne/logiops). Requires apt installing libudev-dev, libevdev-dev, libconfig++-dev, snap installing cmake --classic and `ln -s /usr/local/lib/libhidpp.so /usr/lib/libhidpp.so`
* Install and configure neomutt (possibly using https://github.com/lukesmithxyz/mutt-wizard)
* https://roboleary.net/vscode/2020/08/05/dont-need-extensions.html
* https://news.ycombinator.com/item?id=24195122
* Look into stoplight studio (API design)
* https://github.com/jmdaly/dotfiles/blob/master/stow/tmux/.tmux.conf
* Zellij
* https://github.com/romkatv/zsh4humans
* https://github.com/arxanas/git-branchless#repair
* https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/ and https://news.ycombinator.com/item?id=31009313
* https://github.com/rcoh/angle-grinder

## VIM
* https://github.com/LunarVim/LunarVim
* https://news.ycombinator.com/item?id=31986851

# Interesting software

## macOS

* **Alfred** 				-- Quick launcher
* **AppTrap**			-- Uninstall programs
* **Bear**				-- Notes
* **Charles Proxy**		-- Replay request, rewrite endpoint, ...
* **Dash**				-- Offline documentation tool
* **Disk Inventory X**		-- Visual treemap showing what uses disk space
* **Dragand**			-- Subtitles
* **Fluor**				-- Change F12 / fn F12 settings depending on the active application
* **F.lux / Shifty**		-- Reduces blue light, shifty gives more control over the built-in functionality of macOS while f.lux is a completely standalone solution
* **IINA**				-- Video
* **iTerm**				-- Terminal
* **Lightshot**			-- Screenshots
* **MacDown**			-- Markdown
* **Medis**				-- Redis client
* **MindNode**			-- Brainstorming
* **Nova (by Panic)**			-- IDE
* **Sequel Pro / TablePlus**			-- MySQL client
* **Spark**				-- E-mail
* **Spectacle**	 		-- Window location shortcuts
* **The Unarchiver**		-- Zip unarchiver
* **Tripmode**			-- Limit apps using internet when using limited capacity internet (for example on train)


## Linux

* **[AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher)**	-- Automatically add desktop entries for `.AppImage` applications
* **Bat**					-- Like `cat` but with line numbers, syntax highlighting, ...
* **Caffeine**			-- Don't suspend when fullscreen window is active (for example a video is playing)
* **[cTop](https://github.com/bcicen/ctop)**				-- Monitor docker containers (memory / cpu usage)
* **[Dog](https://github.com/ogham/dog)	-- Get DNS information on command line
* ~~**Earlyoom**~~			-- Kill some applications early before running out of memory (can set which you prefer to kill and which you don't). Later it'll probably be better to switch to facebook's PSI and oomd helper, but these are not merged into the mainline linux kernel just yet
* **Evolution mail + evolution-ews** -- E-mail
* **Ffsend**				-- Easily share files with other people, including expiry time, max amount of downloads, ...
* **Flameshot**			-- Screenshots
* **Flatpak + gnome-software-plugin-flatpak**	-- More software installation options
* **Fzf, ripgrep**			-- Easily find things in a list (fzf keybindings can be particularly useful in conjunction with zsh history searching)
* **Gimp / Kritza**				-- Photoshop alternative
* **GPaste**				-- Clipboard manager
* **Guake**				-- Drop-down terminal
* **Gnome tweaks tool**	-- More customization (themes, fonts, ...)
* **Jumpapp**				-- Quickly switch applications using keyboard shortcuts
* **KVM / QEMU**			-- Virtual machines
* **LibreOffice**			-- Microsoft Office alternative
* **ngrok**				-- Make local url's accessible from other devices. An alternative is Serveo, which you can even host yourself / use on your own domain name for free. Might be interesting to take a look at?
* **Pause-on-lock**		-- Pause spotify when laptop is locked
* **Peek**				-- Make GIFs
* **Station + Slack (for screen sharing)**	-- Station is an alternative to Rambox
* **Sublime Text**		-- Text editing
* **SubSync**			-- Sync subtitles to a video (tries to transcribe what's said in a video using a dictionary and then tries to match it with the translation file)
* **TablePlus / Beekeeper Studio**	-- MySQL client
* **Tldr**				-- Better help than `man`
* **Tlp / Slimbook battery**			-- Power management
* **Trimage**				-- Image compression
* **Typora**				-- Markdown
* **Vivaldi-snapshot**	-- Browser
* **youtube-dl**			-- Download youtube videos

#### Things to look into:

* **nushell**
* **Sequeler**				-- SQL client
* **Azure Data Studio**			-- MSSQL client

### Gnome extensions

* Apt Update Indicator
* Calendar improved (disabled)
* Caffeine
* Dash to Panel
* Force quit
* GPaste
* Gtk title bar / No title bar (depends which version works here)
* Impatience (disabled since I disabled animations)
* NoAnnoyance v2
* Panel OSD
* Remove alt+tab delay v2
* Remove Dropdown Arrows
* Remove rounded corners (disabled)
* Status area horizontal spacing
* Tray icons: reloaded (not really needed, might disable it)
* User Themes
* Window Corner Preview (disabled)
* Windowoverlay icons
* System-monitor

NOTE: do not install Coverflow Alt-tab, [you can do the same without that gnome extensions](https://blogs.gnome.org/fmuellner/2018/10/11/the-future-of-alternatetab-and-why-you-need-not-worry/)

### Tweaks

Applications: Layan-dark
Cursor: Vimix-cursors
Icons: Tela-dark 
Shell: Layan-dark

### Guake / Cmder settings

GTK theme: Layan Dark  
Prefer dark theme  
Default interpreter: /usr/bin/zsh  
Font: RobotoMono Nerd Font Medium 12 / Ioveska Nerd Font 16  
Built-in schemes: Tango / Babun 

### Ubunut settings

* Appearance
	* Auto-hide the dock: true
* Power
	* Bluetooth: off
	* Automatic Brightness: false

## Windows

* **7-zip**				-- Zip (un)archiver
* **Chocolatey**			-- Package manager (similar to Homebrew for mac)
* **Inkdrop**				-- Notes
* [**Files UWP**](https://www.microsoft.com/en-us/p/files-uwp-preview/9nghp3dx8hdx)				-- File explorer
* **KatMouse**			-- Scroll the window you hover over, not the active window
* **Malwarebytes**		-- Malware protection
* **Quicklook / Seer Pro**	-- File preview when pressing space in explorer
* **Recuva**				-- File recovery
* **ShareX**				-- Screenshots
* **UnChecky**			-- Automatically uncheck unerlated offers from installers
* **Velocity**				-- Offline documentation tool
* **Windows 10 PowerToys**
* **Windows Terminal**		-- Terminal
* **WizTree**			-- Visual treemap showing what uses all of the disk space

# PHPStorm plugins

* .env files support
* .ignore
* Atom material icons
* Automatic Power Saver
* BashSupport
* CamelCase (?)
* deep-assoc-completion
* deep-js-completion
* GitToolBox
* Ideolog (?)
* Import Cost
* IntelliJ Gitlab Pipeline Viewer (?)
* IntelliVue (?)
* JS Inspections (EA Ultimate)
* Key Promoter X
* Laravel
* Laravel idea
* Markdown Navigator (?)
* Material Theme UI
* OpenAPI Specifications (?)
* NEON support (?)
* PHP Annotations
* PHP composer.json support
* PHP Inspections (EA Extended)
* PHP Toolbox
* PHPUnit Enhancement (?)
* Rainbow Brackets
* RegexpTester (?)
* Shared Indexes (?)
* ~~SonarLint~~
* String Manipulation
* Symfony Support
* Tailwind Formatter
* Unit File Support (systemd)
* WakaTime
* YAML/Ansible support

## Interesting links

* https://git.sr.ht/~whereswaldon/presentations/tree/master/terminal-velocity-ato-2019.md
* https://www.reddit.com/r/rust/comments/i1abpg/rewritten_in_rust_modern_alternatives_of/
* https://github.com/kbknapp/dotfiles/blob/master/bin/common/rust.sh

## Known issues

### Not all of my PHPstorm keyboard shortcuts are working (`CTRL+SHIFT+E`, `CTRL+SHIFT+U`, ...)

* In ubuntu search for `Language Support`
* Change `Keyboard input method system` to `XIM`
* Log out or reboot

### My touchpad has a middle mouse click which I don't want

First find the correct input device

`xinput list`

Then you can see which inputs the device supports

`xinput get-button-map <device id or name>`

Then you can change the actual mapping. Change the specific button to 0 (disabled) or 1 (left mouse click). Middle mouse button will probably be either the second or third button in the mapping. Example for my current touchpad:

`xinput set-button-map <device id or name> 1 1 3 4 5 6 7`

Place these settings in `~/.xsessionrc` to persist them across reboots.
