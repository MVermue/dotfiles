#! /usr/bin/env bash

# Stop on error
set -e

echo 'Installing sources'
echo '------------------'
sudo apt-get install -y ca-certificates curl software-properties-common
sudo mkdir -p /etc/apt/keyrings

echo 'Adding Docker source'
wget -q0 - https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo 'Adding sublimetext source'
wget -q0 - https://download.sublimetext.com/sublimehq-pub.gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/sublimehq-archive.gpg
sudo add-apt-repository "deb https://download.sublimetext.com/ apt/stable/" -y

echo 'Adding TablePlus source'
wget -q0 - http://deb.tableplus.com/apt.tableplus.com.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/tableplus.gpg
sudo add-apt-repository "deb [arch=amd64] https://deb.tableplus.com/debian tableplus main" -y

echo 'Add go source'
sudo add-apt-repository ppa:longsleep/golang-backports -y

echo 'Adding alacritty source'
sudo add-apt-repository ppa:aslatter/ppa -y

echo 'Adding stacer source'
sudo add-apt-repository ppa:oguzhaninan/stacer -y

echo 'Adding nala source'
echo "deb [arch=amd64,arm64,armhf] http://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null

echo 'Installing packages'
echo '-------------------'
sudo apt-get update
sudo apt-get install -y \
	alacritty \
	ansible `# ansistrano` \
	byobu \
	build-essential \
	chrome-gnome-shell \
	docker-ce \
	duf \
	evolution evolution-ews \
	fd-find \
	flatpak gnome-software-plugin-flatpak \
	gimp \
	gir1.2-gtop-2.0 gir1.2-nm-1.0 gir1.2-clutter-1.0 `# gnome system monitor extension` \
	gnome-tweaks \
	golang-go `# cod` \
	gpaste gnome-shell-extensions-gpaste \
	jq \
	libgtk2.0-0 `# needed for sublime text` \
	lua5.3 \
	nala \
	peek \
	python3-pip \
	qt5-style-plugins \
	ripgrep \
	stacer \
	stow \
	tableplus \
	tlp tlp-rdw \
	tree \
	ubuntu-restricted-extras \
	vim \
	vlc \
	zsh \
	xdotool wmctrl libinput-tools # libinput-gestures
snap install ffsend gallery-dl layan-themes ngrok postman raven-reader redis-desktop-manager subsync tela-icons # Although it's tempting don't install mysql-workbench-community from snap, it has font bugs etc.
snap install --classic code

sudo pip3 install --upgrade \
	asciinema

#pip3 install --user pynvim # vim deoplete

echo 'Setting up flatpak remote'
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install com.github.alainm23.planner com.github.geigi.cozy com.spotify.Client org.filezillaproject.Filezilla org.onlyoffice.desktop-editors org.qbittorrent.qBittorrent com.github.huluti.Curtail com.slack.Slack com.flameshot.Flameshot com.github.qarmin.czkawka md.obsidian.Obsidian com.usebottles.bottles

echo 'Setting up Linuxbrew'
bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
echo 'PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:\$PATH"' >> ~/.profile
export PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH"

echo 'Installing brew packages'
brew tap dandavison/delta https://github.com/dandavison/delta
brew install \
  bat `# better version of cat` \
  bandwhich `# check which process uses how much bandwith` \
  bit-git `# git with autocomplete` \
  croc `# encrypted resumeable file transfer` \
  ctop `# docker container usage` \
  dog `# dns lookup` \
  dust `# visualise disk usage` \
  exa `# ls alternative, possible others are colorls (used before) or lsd` \
  fzf \
  git-absorb `# fix up commits` \
  git-delta `# diff tool` \
  `# grex # tool to generate regexes from input (not really using it, but it is cool` \
  hyperfine `# benchmarking tool` \
  `# navi # cheatsheets` \
  noti \
  profclems/tap/glab \
  pueue \
  sshrc \
  tealdeer \
  thefuck \
  vivid `# style commands with colors (du, ls, ...)`
#  xxh `# this still requires some fixes to TERM (https://stackoverflow.com/questions/17627193/backspace-in-zsh-fails-to-work-in-quite-the-strange-way and https://news.ycombinator.com/item?id=27075659)`
$(brew --prefix)/opt/fzf/install

bit complete

echo 'Installing docker-compose'
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo 'Set up docker so unprivileged users can run it'
sudo usermod -aG docker $USER

echo 'Creating some directories commonly mounted in docker'
mkdir ~/.composer ~/.npm

echo 'Installing pause-on-lock'
sudo curl -L "https://github.com/folixg/pause-on-lock/releases/latest/download/pause-on-lock" -o /usr/local/bin/pause-on-lock
sudo chmod +x /usr/local/bin/pause-on-lock

# TODO: installing gnupg, gnome extensions
# TODO: make ~/.profile_local get properly loaded from our ~/.profile file
# TODO: install btop

#echo 'Make applications use GTK2 style'
#echo "export QT_QPA_PLATFORMTHEME=gtk2" >> ~/.profile

echo 'Setting up config files'
mkdir ~/.byobu # this is just so byobu config files are symlinked individually, not the entire directory
stow alacritty byobu git htop home ssh zsh
sudo stow --target=/etc etc

echo 'Installing docker developer hosts'
echo '---------------------------------'
sudo systemctl enable docker.developer-hosts.service
sudo systemctl start docker.developer-hosts.service

echo 'Set some default settings'
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.interface enable-animations false
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot '[]' # because we will bind flameshot to printscreen instead

echo 'Installing f2 (multi rename)'
curl -fL https://github.com/ayoisaiah/f2/releases/download/v1.6.4/f2_1.6.4_linux_amd64.tar.gz | tar -xvz
chmod +x f2
sudo mv f2 /usr/local/bin

echo 'Installing touch gestures'
sudo gpasswd -a $USER input # Add user to input group
sudo git clone https://github.com/bulletmark/libinput-gestures.git /opt/libinput-gestures

# Remaining steps are done in post-install
