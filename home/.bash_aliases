alias help='tldr'
alias cat='bat'
alias ls='exa -aF --icons --color=always '
alias list-installed='zless /var/log/dpkg.log* | grep "install "'
alias list-upgraded='zless /var/log/dpkg.log* | grep "upgrade "'
alias list-removed='zless /var/log/dpkg.log* | grep "remove "'
alias fd='fdfind'
alias srm='rm -I'
alias disk-analyze='docker system info'
alias install="apt-cache search '' | sort | cut --delimiter ' ' --fields 1 | fzf --multi --cycle --reverse --preview 'apt-cache show {1}' | xargs -r sudo apt install -y" # Source: https://github.com/jmdaly/dotfiles/issues/3
alias i="install"
alias ssh="TERM=xterm-256color ssh"
alias xxh="TERM=xterm-256color xxh"
